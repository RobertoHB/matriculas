<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tutores */


$this->title = 'Actualizando Tutor';
$this->params['breadcrumbs'][] = ['label' => 'Tutores', 'url' => ['index']];


    echo Html::button('Volver', array(
           'name' => 'btnBack',
           'class' => 'uibutton loading confirm',
           'style' => 'width:100px;',
           'onclick' => "history.go(-1)",
               )
       );


 

//$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
//$this->params['breadcrumbs'][] = ['label' => 'Volver', 'url' =>Yii::$app->request->referrer ?: Yii::$app->homeUrl];

$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tutores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
