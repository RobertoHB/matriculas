<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Alumnos;

/**
 * AlumnosSearch represents the model behind the search form of `app\models\Alumnos`.
 */
class AlumnosSearch extends Alumnos
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_escolar', 'expe_centro', 'cp', 'tel_fijo', 'movil', 'discapacidad','f_proteccion'], 'integer'],
            [['passnie','dni', 'nombre', 'apellidos', 'f_nac', 'loc_nac', 'prov_nac', 'domicilio', 'localidad', 'provincia', 'centro_ant', 'tit_acceso', 'foto', 'email', 'nacionalidad'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Alumnos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_escolar' => $this->id_escolar,
            'expe_centro' => $this->expe_centro,
            'f_nac' => $this->f_nac,
            'cp' => $this->cp,
            'tel_fijo' => $this->tel_fijo,
            'movil' => $this->movil,
            'discapacidad' => $this->discapacidad,
        ]);

        $query->andFilterWhere(['like', 'dni', $this->dni])
            ->andFilterWhere(['like', 'passnie', $this->passnie])
            ->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'apellidos', $this->apellidos])
            ->andFilterWhere(['like', 'loc_nac', $this->loc_nac])
            ->andFilterWhere(['like', 'prov_nac', $this->prov_nac])
            ->andFilterWhere(['like', 'domicilio', $this->domicilio])
            ->andFilterWhere(['like', 'localidad', $this->localidad])
            ->andFilterWhere(['like', 'provincia', $this->provincia])
            ->andFilterWhere(['like', 'centro_ant', $this->centro_ant])
            ->andFilterWhere(['like', 'tit_acceso', $this->tit_acceso])
            ->andFilterWhere(['like', 'foto', $this->foto])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'nacionalidad', $this->nacionalidad]);
        

        return $dataProvider;
    }
}
