<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tutores".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property string|null $dni
 * @property int|null $telefono
 */
class Tutores extends \yii\db\ActiveRecord
{
    public $lista_tutores;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tutores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['telefono'], 'integer'],
            [['nombre','lista_tutores'], 'string', 'max' => 100],
            [['apellidos'], 'string', 'max' => 200],
            [['dni'],'string', 'max' => 10],
            [['observaciones'],'string'],
            [['apellidos','nombre'], 'required', 'when' => function($model) {
                            return !empty($model->apellidos);
                            }],
                         
//             [['nombre','apellidos'],'unique','tutorDuplicado'],
//                                   [['apellidos','nombre'], 'unique', 'when' =>function($model) {
//                        if (Tutorduplicado($this->nombre,$this->apellidos)) {
//                            $this->addError($model, 'This email is already in use".');
//                        }
//                    }],
                               
                            
              //[['nombre', 'apellidos'], 'comboNotUnique' => 'Tutor ya existe', 'unique', 'targetAttribute' => ['nombre', 'apellidos']]
              [['nombre','apellidos'], 'unique', 'skipOnError' => true, 'targetClass' => Tutores::className(), 'targetAttribute' => ['nombre' => 'nombre','apellidos'=>'apellidos']],
        ]; 
       
//            [['nombre'], 'required'],
//            [['apellidos'], 'required'],
//            [['nombre', 'apellidos'], 'unique', 'targetAttribute' => ['nombre', 'apellidos']]
//            ['apellidos', 'validateTutor'],
           

//          return array(  
//            array('nombre', 'unique',
//
//                 'criteria' => array(
//
//                     'condition' => 'nombre= :nombre',
//
//                     'params' => array(':nombre' => $this->nombre))),
//
//             array('apellidos', 'unique',
//
//                 'criteria' => array(
//
//                     'condition' => 'apellidos= :apellidos',
//
//                     'params' => array(':apellidos' => $this->apellidos))),
//        ); 
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'dni' => 'Dni',
            'telefono' => 'Telefono',
            'observaciones' => 'Observaciones',
        ];
    }
     
    public function validateTutor($attribute,$params)

    {


      if(Tutores::model()->exists('nombre=:nombre AND apellidos=:apellidos',array(

                    ':nombre'=>$this->nombre,

                    ':apellidos'=>$this->apellidos

                  )))

               $this->addError($attribute,"You have your tutor set already");





   }
     public function validarTutor(){
       
//         if (self::find()->where(['nombre' => $this->nombre])
//                 ->andWhere(['apellidos'=> $this->apellidos])->count()>0) {
//                $this->addError('apellidos','Tutor ya existe.');
//                  
//        }
       
              $this->addError('apellidos', 'Incorrect Tutor.');
         
//          $nombre = Tutores::find($this->nombre);
//          $apellidos = Tutores::find($this->apellidos);
//
//        if ($nombre && $apellidos) {
//            $this->addError('apellidos', 'Incorrect Tutor.');
//        }
    }
   
    public function tutorDuplicado($nombre,$apellidos){
       
         if (Tutores::find()->where(['nombre' => $nombre])
                 ->andWhere(['apellidos'=> $apellidos])->exists()) {
                    
                    return true;
        }
    }
    
    public function getResponsables(){
        return $this->hasOne(Responsables::className(), ['i_tutor' => 'id']);
       
   }
//    public function validateDni($attribute,$params)
//
//        {  
//        if(Tutores::find()->exists('dni=:dni',array('dni'=>$this->dni)))
//
//             $this->addError('dni','dni incorrecto o ya existe');
//        
//        }
//    public function validateDni($model)
//    {
//        $value = $model->dni;
//        if (Tutores::find()->where(['dni' => $value])->exists()) {
//            $model->addError($attribute, $this->message);
//        }
//    }
}
