<?php

namespace app\models;

use Yii;
    

/**
 * This is the model class for table "responsables".
 *
 * @property int $id
 * @property int|null $id_tutor
 * @property string|null $dni_alumno
 *
 * @property Alumnos $dniAlumno
 */
class Responsables extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'responsables';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_tutor'], 'integer'],
            [['dni_alumno'], 'string', 'max' => 10],
            [['dni_alumno'], 'exist', 'skipOnError' => true, 'targetClass' => Alumnos::className(), 'targetAttribute' => ['dni_alumno' => 'dni']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_tutor' => 'Id Tutor',
            'dni_alumno' => 'Dni Alumno',
        ];
    }

    /**
     * Gets query for [[DniAlumno]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDniAlumno()
    {
        return $this->hasOne(Alumnos::className(), ['dni' => 'dni_alumno']);
    }
     public function getAlumno()
    {
        return $this->hasOne(Alumnos::className(), ['dni' => 'dni_alumno']);
    }
    
   public function getTutor0(){
        return $this->hasOne(Tutores::className(), ['id' => 'id_tutor']);
       
   }
    public function getTutores(){
       return $this->hasOne(Tutores::className(), ['id' => 'id_tutor']);
       
   } 
   
   //creados para que coincidan las tablas con las relaciones en informe dinámico
    public function getAlumnos()
    {
        return $this->hasOne(Alumnos::className(), ['dni' => 'dni_alumno']);
    }
   
   
   
   
   //--------------------------------------------
    
    
}
