<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "datosbancarios".
 *
 * @property int $id
 * @property string|null $banco
 * @property string|null $iban
 * @property string|null $titular
 * @property string|null $dni
 *
 * @property Matriculas[] $matriculas
 */
class Datosbancarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'datosbancarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['banco'], 'string', 'max' => 200],
            [['iban'], 'string', 'max' => 24,'min'=>24],
            [['titular'], 'string', 'max' => 200],
            [['dni'], 'string', 'max' => 9],
           
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'banco' => 'Entidad',
            'iban' => 'Iban',
            'titular' => 'Titular',
            'dni' => 'Dni',
          
        ];
    }

    /**
     * Gets query for [[Matriculas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMatriculas()
    {
        return $this->hasMany(Matriculas::className(), ['id_datos_bancarios' => 'id']);
    }
    
    
     public function getDatosbancariosduplicado($iban){
        $model = Datosbancarios::find()
                ->where(['iban' => $iban])
                ->all();  

       if (count($model)>0) {
           return($model[0]["id"]);
       }else{
           return(0);
       }
    }
}
