<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\ActiveField;
use yii\helpers\Url;
use yii\captcha\Captcha;
/* @var $this yii\web\View */
/* @var $model app\models\Clientes */
/* @var $form yii\widgets\ActiveForm */


?>



<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css"></script>
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="<?= Url::to('@web/css/firma.css')?>">

<script src="<?= Url::to('@web/js/firma_electronica.js')?>"></script>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>

<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E="></script>



<?php


$this->title = 'Firma electronica';
$this->params['breadcrumbs'][] = $this->title;
?>
 
<div id="contenedorCanvas" class="container-fluid" width="100%">
<!--      <span id="cerrar_Ventana_firma" style="text-align: right;"><i class="far fa-times-circle fa-2x" id="cerrar_firma"></i><span>-->
      <h3>Firma Matrícula</h3>
      <canvas width="300" height="200" style="border: none;border-radius:5%">
      
      </canvas>

      <div id="contenedor_imag" class="container-fluid" hidden>
      </div>  

      <div id="botones">
           <button id="borrar"><i class="fas fa-times fa-2x"></i></button>
           <button id="validar"><i class="fas fa-check fa-2x"></i></button>
           <button id="confirmar"><i class="fas fa-share fa-2x"></i></button>
       </div>

</div>


