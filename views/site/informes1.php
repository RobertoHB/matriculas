<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;




$this->title = 'Generador de Informes';
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    
   .contiene_check{
       padding-left:5px;
       text-align: left;
      
    }
    .item{
        font-size: 16px;
        margin-top: 5px;
        width:290px;
        
    }
    .tableFixHead          { overflow-y: auto; height:700px; }
    .tableFixHead thead th { position: sticky; top: 0; }

    /* Just common table stuff. Really. */
    table  { border-collapse: collapse; width: 100%;}
    th, td { padding: 2px 4px; }
    th     { background:#eee; }
    
    select {
    font-family: 'Lato', 'Font Awesome 5 Free';
    font-weight: 900;
}
    
</style>
    
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<div class="alumnos-index">
    <div class="container-fluid">
        <div class = "row"><!--<h1><?= Html::encode($this->title) ?></h1>-->
            <div class="col col-sm-4" style="border:1px solid blanchedalmond;border-radius: 20px;width:310px;">
                <div class="datos item btn btn-primary">
                    <span style="">Datos<i class="fa fa-angle-down" id="datos"></i></span>
                    <div id="op_datos" class="op" style="border:1px solid black;border-radius:10px;display:block;width:200px;margin-left:2px;padding-left:2px;">
                        
                        
                        
                    </div>
                </div>    
                <div class="campos item btn btn-primary">
                    <span>Campos<i class="fa fa-angle-down" id="campos"></i></span>
                       <div id="op_campos" class="op" >

                    </div>
                </div> 
                <div class="criterios item btn btn-primary">
                    <span>Criterios<i class="fa fa-angle-down" id="criterios"></i></span> 
                   <div id="op_criterios" class="op" >
<!--                      <select name="criterios" id="lista_criterios" style="color:black;width:125px;font-size: 12px;height: 22px;">
                       
                      </select>
                       <select name="operacion" id="operacion" align="center" style="color:black;text-align:center">
                        <option value="=">=</option> 
                        <option value="<>" selected><></option>
                        <option value=">">></option>
                          <option value=">"><</option>
                      </select>
                       <input type="text" id="compara-criterio" class="" size="7" style="height:22px;color:black;font-size: 12px;"/>-->
                       <span><i class="fas fa-plus fa-1x" id="anadir_criterio" style="color:white"></i></span>
                    </div>     
                </div>  
                <div class="grupos item btn btn-primary">
                    <span>Grupos<i class="fa fa-angle-down"></i></span>
                    <div id="op_grupos" class="op" >

                    </div>     
                </div>
                <div class="orden item btn btn-primary">
                    <span>Orden<i class="fa fa-angle-down"></i></span>
                    <div id="op_orden" class="op" >

                    </div>     
                </div> 
                <div class="titulo item btn btn-primary">
                    <span>titulo<i class="fa fa-angle-down" id="titulo"></i></span>
                     <div id="op_titulo" class="op" >
                         <input id ="titulo" type="text" size="27" style="color:black" />
                    </div>     
                </div> 
                  <div class="salida item btn btn-primary">
                    <span>Salida<i class="fa fa-angle-down" id="salida"></i></span>
                     <div id="op_salida" class="op" >
                        <select name="salida" id="salida" align="center" style="color:black;text-align:center">
                             
                            <option value="0" selected>&#xf2d0; Pantalla </option>
                            <option value="1">&#xf1c3; Excel </option>
                            <option value="2">&#xf1c1; Pdf</option>
                           
                        </select>
                    </div>
                    
                </div> 

                <div class="" style="margin-top:20px;margin-bottom: 20px;">
                    <button class="btn btn-block btn-lg" id="filtrar">Filtrar</button>
<!--                     <a type="button"  href="<?=url::to('../site/excel_informe_dinamico/')?>" class="btn btn-info btn-group-sm" id="">
                        Excel 
                    </a>-->
                </div>
            </div>     
  
       
     
           <div class="col col-sm-8">
               <div class="listado tableFixHead" id="listado">
             
               </div> 
           </div>  
        </div>
    </div>   
</div>
<script>
    $( document ).ready(function() {
        datos_tablas();
        $( ".op" ).toggle();
        $('i').click(function(event) { 
//            console.log("clik");
            let elemento = "#op_"+event.target.id;
//            console.log(elemento);
           $(""+elemento+"").toggle( "slow", function() {
       // Animation complete.
            });
            
        });
        //controlamos el toggle de los elementos que contienen los campos agrupados por tabla
        $("div#op_campos").delegate('i','click', function(event) { 
          
             let elemento = "#op_"+event.target.id;
           
           $(""+elemento+"").toggle( "slow", function() {
       // Animation complete.
            });
            
            
        });

        //enviamos el array con todas las tablas y los campos,criterios,agrupaciones,orden y titulo por ajax al controlador
        $('button#filtrar').click(function(event) { 
            var obj = {};
            obj['tablas'] = {};
            obj['campos'] = {};
            obj['criterios'] = {};
            obj['titulo'] = {};
            obj['salida'] = {};
            
            
//              $("input:checkbox:checked").each(function() {
//                checkeados = [$(this).attr('id')];
//           });
//          
           var checkeados = document.querySelectorAll('input[type="checkbox"]:checked');
          
           for(let i=0;i < checkeados.length;i++){
               
               if (checkeados[i].className == 'tablas'){
                  
                   obj['tablas'][i] = checkeados[i].id;                   
                  
                }else{
                     obj['campos'][i] = checkeados[i].id;
               }
               
               
           }
           
          
           //Vamos creando el array de criterios con el select criterio el select operacion y el input valor
            var criterio = document.querySelectorAll('div select#lista_criterios option:checked');
            var operacion = document.querySelectorAll('div select#operacion option:checked');
            var valor = document.querySelectorAll('div input#compara-criterio');
            let cadena_criterio;
//            console.log(criterio,operacion,valor);        
            for(let i=0;i < criterio.length;i++){
                cadena_criterio = criterio[i].innerText;
                cadena_criterio += operacion[i].innerText;
                cadena_criterio += '"'+valor[i].value+'"';
                obj['criterios'][i] = cadena_criterio;
//                obj['criterios'][i] = "'"+cadena_criterio+"'";
            }
            
            
//            Creamos el array de titulo
            var titulo = document.querySelector('div input#titulo');

            obj['titulo'][0] = titulo.value;
            
//          creamos el array de tipo de salida de informe (pantalla,excel,pdf....)

            var salida = document.querySelector('div select#salida option:checked');
          
                obj['salida'][0] = salida.value;
        
                
                
            
           //enviamos por ajax el array con las tablas y los campos selecionados dependiendo del valor
           if(obj['salida'][0] == 0) generar_informe(obj);
//           window.location.href = '@Url.Action("MyMethod", "SomeControllerName", new { area = "MyArea", id = Model.MyId })';
        
         
           if(obj['salida'][0] == 1)   
               window.location.href= "<?= url::to('@web/site/excel_informe_dinamico'); ?>" + "?datos="+JSON.stringify(obj); 
        });
        
        
        //controlamos la deseleccion de un input type check en tablas
         $("div#op_datos").delegate('input','change', function(event) { 
             
            if(event.target.checked){
                 crear_campos_tabla($(this).attr('id'));
            }else{
                 eliminar_campos_tabla($(this).attr('id'));
          }     
        });
        
        //controlamos la deseleccion de un input type check en campos
         $("div#op_campos").delegate('input','change', function(event) { 
             
            if(event.target.checked){
                 crear_option_criterios($(this).attr('id'));
            }else{
                 eliminar_option_criterios($(this).attr('id'));
          }     
         

        });
         
        $("#anadir_criterio").click(function() { 
           let id_select = Math.floor(Math.random() * 11); 
           $('#op_criterios').append('<div id="op_criterios" class="op" align="left"><select name="'+id_select+'" id="lista_criterios" style="color:black;width:125px;font-size: 12px;"></select><select name="operacion" id="operacion" style="color:black;text-align:center;margin-left:4px"><option value="=">=</option><option value="<>" selected><></option><option value=">">></option> <option value="<"><</option></select> <input type="text" id="compara-criterio" class="" size="7" style="height:22px;margin-left:1px;color:black;font-size: 12px;"/><span><i class="fas fa-minus fa-1x criterio" id="anadir_criterio"  style="color:red;margin-left:4px;"></i></span></div>');                   
           llenar_option_criterios(id_select);                   
                    
        });
        //$('#anadir_criterio').click(function() { 
            $('body').on('click', 'i.criterio', function() {
      
          $(this).parent().parent().remove();
        });       
}); 

    function datos_tablas(){
        var baseUrl = "<?= Url::toRoute(["/"]); ?>"; 
            $.ajax({

                type: "GET",
                dataType: "json",
              
                url: baseUrl + "site/datostablas",

                data: {
                   
                }

            })
                 .done(function (data) {
                    $.each( data, function( key, value ) {    
                        let valor = value.charAt(0).toUpperCase() + value.slice(1);
                        $('#op_datos').append('<div style="display:block" class="contiene_check"><input type="checkbox" class="tablas" id="'+valor+'"/>' + valor+ '</div>'); 
                  });
                    //  console.log(data)
           //creamos los elementos correspondientes a las tablas
                
            }); 
    
    }
    function eliminar_campos_tabla(mitabla){
        $('div#tbl_'+mitabla+'').empty();
     }
    function crear_campos_tabla(mitabla){
    //vaciamos todos los elementos anteriores    
       
        var baseUrl = "<?= Url::toRoute(["/"]); ?>"; 
        var mi_tabla = mitabla;
        
      
            $.ajax({

                type: "GET",
                dataType: "json",
              
                url: baseUrl + "site/campostablas",

                data: {
                   tabla: mi_tabla
                }

            })
                 .done(function (data) {
                     $('#op_campos').append('<div id="tbl_'+mi_tabla+'"><span style="font-size:10px;">' + mi_tabla + '<i class="fa fa-angle-down" id="'+mi_tabla+'"></i></span><div id="op_'+mitabla+'" class="op" style="border:1px solid black;border-radius:10px;display:block;width:200px;margin-left:5px;padding-left:5px;">');
                    $.each( data, function( key, value ) {    
                        let valor = value;
                        $('#op_'+mitabla).append('<div style="display:block" class="contiene_check"><input type="checkbox" class="campos" id="'+mitabla.toLowerCase()+'.'+valor+'"/>' + valor); 
                         $('#op_'+mitabla).append('</div></div>'); 
                  });
                    //  console.log(data)
           //creamos los elementos correspondientes a las tablas
                
            }); 
    
    }
    
    
    function crear_option_criterios(campo){
//        var campos_check_criterios = document.querySelectorAll('input.campos[type="checkbox"]:checked');          
//        for(let i=0;i < campos_check_criterios.length;i++){
        //creamos los inputs de criterios a partir de los campos creados
           $('#lista_criterios').append('<option class="criterio" id="'+campo+'" value="'+campo+'" style="color:black;font-size:12px;height: 22px;">'+campo+'</option>'); 
//        }
    }
    function eliminar_option_criterios(campo){
//        var campos_check_criterios = document.querySelectorAll('input.campos[type="checkbox"]:checked');          
//        for(let i=0;i < campos_check_criterios.length;i++){
        //creamos los inputs de criterios a partir de los campos creados
       // $("#lista_criterios option[value=''+campo+'']").remove();
        $('#lista_criterios option[value = "'+campo+'"').remove();
       
           //$('#lista_criterios').append('<option class="criterio" value="'+campo+'" style="color:black">'+campo+'</option>'); 
//        }
    }
    
    function llenar_option_criterios(nselect){
        var campos_checkeados = document.querySelectorAll('input.campos[type="checkbox"]:checked');
       

          for(let i=0;i < campos_checkeados.length;i++){
           
            $('#lista_criterios[name="'+nselect+'"]').append('<option class="criterio" id="'+campos_checkeados[i].id+'" value="'+campos_checkeados[i].id+'" style="color:black;font-size: 12px;height: 22px;">'+campos_checkeados[i].id+'</option>');
          }
        
    
    }
    
    
    function generar_informe(datos){
   
        var baseUrl = "<?= Url::toRoute(["/"]); ?>"; 
        var datos_consulta = datos;
            $.ajax({

                type: "POST",
//                dataType: "json",
              
                url: baseUrl + "site/informeauto",
              
                data: {
//                   datos: JSON.stringify(tablasCampos)
                    datos: datos_consulta
                }
          
    
            })
            .done(function (data) {
               // console.log(data);
            $('div.listado').html(data);
//               $.pjax.reload({container:'#lista_informe'});
             });

    
    }
    
       
</script>