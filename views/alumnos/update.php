<?php

use yii\helpers\Html;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model app\models\Alumnos */

$this->title = 'Actualizar Alumno: ' . $model->dni;
$this->params['breadcrumbs'][] = ['label' => 'Alumnos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dni, 'url' => ['view', 'id' => $model->dni]];
$this->params['breadcrumbs'][] = 'Actualizar';

?>
 
<div class="alumnos-update">
  
      
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
