<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Datosbancarios */

$this->title = 'Create Datosbancarios';
$this->params['breadcrumbs'][] = ['label' => 'Datosbancarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="datosbancarios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
