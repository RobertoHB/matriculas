<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use app\models\Alumnos;
use app\models\AlumnosSearch;
use app\models\Responsables;
use app\models\Tutores;
use yii\helpers\Url;
use yii\jui\AutoComplete;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ResponsablesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$itemTutores = ArrayHelper::map(Tutores::find()->all(), 'id', 'apellidos','nombre','dni');

$this->title = 'Responsables';
$this->params['breadcrumbs'][] = $this->title;


//        $nombres = array_column($datosAlumno, 'nombre');
//        print_r($nombres);
//
//exit;

?>


<div class="responsables-index">

    <h3>Alumno</h3>
    
 <?= GridView::widget([
        'dataProvider' => $datosAlumno,
        'summary' => '',
        //'filterModel' => $searchModel,
        'columns' => [
            'dni',
            'nombre',
            'apellidos',
        ],
    ]); ?>
   

   
 <h1><?= Html::encode($this->title) ?></h1>
 
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
         'summary' => '',
        //'filterModel' => $searchModel,
        'columns' => [
            //'id',
            //'id_tutor',
            [
              'label' => 'Nombre',
              'attribute' => 'id_tutor',
               'value' => 'tutor0.nombre',
//                'filter' => ArrayHelper::map(Alumnos::find()->all(), 'id', 'nombre'),
//              'enableSorting' => true,
            ],
             [
              'label' => 'Apellidos',
              'attribute' => 'id_tutor',
               'value' => 'tutor0.apellidos',
//                'filter' => ArrayHelper::map(Alumnos::find()->all(), 'id', 'nombre'),
//              'enableSorting' => true,
            ],
             [
              'label' => 'D.N.I',
              'attribute' => 'id_tutor',
               'value' => 'tutor0.dni',
//                'filter' => ArrayHelper::map(Alumnos::find()->all(), 'id', 'nombre'),
//              'enableSorting' => true,
            ],
            [
              'label' => 'Telefono',
              'attribute' => 'id_tutor',
               'value' => 'tutor0.telefono',
//                'filter' => ArrayHelper::map(Alumnos::find()->all(), 'id', 'nombre'),
//              'enableSorting' => true,
            ],
              [
              'label' => 'Observaciones',
              'attribute' => 'id_tutor',
               'value' => 'tutor0.observaciones',
//                'filter' => ArrayHelper::map(Alumnos::find()->all(), 'id', 'nombre'),
//              'enableSorting' => true,
            ],
            //'dni_alumno',

           
            ['class' => 'yii\grid\ActionColumn',
                
                'contentOptions' => ['style' => 'width:60px;'],
                'header'=>'Acciones',
                'template' => '{update}{delete}',
                'buttons' => [

                                'update' => function ($url,$model) {
                                return Html::a(
                                    '<span class="glyphicon glyphicon-pencil"></span>',Url::to('@web/tutores/update'),
                                        ['data'=>[
//
                                                    'method' => 'get',
//
                                                    'params'=>['id'=>$model->id_tutor,'alumno'=>$model->dni_alumno],
//
                                                    ]
                                            
                                            ]);
                                 },

                                'delete' => function ($url, $model, $key) {

                                    return  Html::a(
                                             '<span class="glyphicon glyphicon-trash" style="padding-left:5px;"></span>',Url::to('@web/responsables/delete'.'?id='.$model->id),
                                                                
                                             ['data'=>[
//
                                                    'method' => 'post',

                                                    'confirm' => 'Are you sure?',
//
                                                    'params'=>['alumno'=>$model->dni_alumno],
//
                                                    ]
                                            
                                            ]);

                                }

                            ]
            ],  
        ],
    ]); ?>


</div>

<!--<div id="buscar_tutor">
   Html::button('Buscar',['class' => 'btn btn-default','onclick'=>'$(".tutores-form").show()'])
</div>-->

<!--<div class="tutores-buscar" style="display:none">-->
     <!--Html::input('select','buscar_tutor','$itemTutores', $options=['class'=>'form-control', 'style'=>'width:350px','prompt'=>''])--> 
<!--     Html::dropDownList($itemTutores,'id', 'nombre'), 
    [
        'style' => 'width:350px !important'
       
       
    ]  
  ;-->


<div id="lista_tutores">
 <?php
    $form = ActiveForm::begin([
           'method' => 'post',
           'id' => 'form_lista_tutores',
           'enableClientValidation' => true,
           'enableAjaxValidation' => false,
           'action'=> Url::to('@web/tutores/datostutor'.'?alumno='.$dniAlumno),
          ]); ?>
    
 <?= $form->field($model, 'lista_tutores')->dropDownList($itemTutores, // Flat array ('id'=>'label')
                                                    ['class'=>'form-control lista_tutores','prompt'=>'Seleccione un Tutor']);  ?> 
<div class="form-group">
   <?= Html::submitButton('Agregar', ['class' => 'btn btn-success',  
                                      'data' => ['confirm' => 'Are you sure ?'],
                                     ]
                         ) ?>


<!-- Html::a('<small class="btn btn-default" role="button">Nuevo Responsable</small>'
               ,array('onclick'=>'$(".tutores-form").show()')
               
             );-->
  <?= Html::button('Nuevo Responsable',['class' => 'btn btn btn-success','onclick'=>'$(".tutores-form").show()']);?>
   
</div>
<?php ActiveForm::end(); ?>
</div>   


<div class="tutores-form container thumbnail" style="display:none;margin-top:50px !important;">

<!--   $data = Tutores::find()
        ->select(['apellidos as value','id as id','apellidos','nombre','dni','telefono','observaciones'])
        ->asArray()
        ->all();

        echo 'tutores' .'<br>';
	echo AutoComplete::widget([    
	'clientOptions' => [
            'source' => $data,
            'minLength'=>'3', 

            'autoFill'=>true,
            'select' => new JsExpression("function( event, ui ) {
                                    $('#nombre').val(ui.item.nombre),
                                     $('#apellidos').val(ui.item.apellidos),
                                      $('#dni').val(ui.item.dni),
                                       $('#telefono').val(ui.item.telefono),
                                        $('#observa').val(ui.item.observaciones);

                                 }")],
        ]);
		   -->
            
            
	
    
     <?php $form = ActiveForm::begin([
            'method' => 'post',
            'id' => 'formulario',
            'enableClientValidation' => true,
            'enableAjaxValidation' => false,
            'action'=> Url::to('@web/tutores/guardartutor'.'?alumno='.$dniAlumno),
           ]); ?>
    
    <div class="form-row">
        <div class="col-sm-2">
            <?= $form->field($model, 'nombre')->textInput(['maxlength' => true,'id'=>'nombre']) ?>
        </div>
        <div class="col-sm-2">
            <?= $form->field($model, 'apellidos')->textInput(['maxlength' => true,'id'=>'apellidos']) ?>
        </div>
   
    
         <div class="col-sm-2">
            <?= $form->field($model, 'dni')->textInput(['maxlength' => true,'id'=>'dni']) ?>
         </div>
         <div class="col-sm-2">
            <?= $form->field($model, 'telefono')->textInput(['id'=>'telefono']) ?>
         </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'observaciones')->textInput(['id'=>'observa']) ?>
        </div>
    </div>   
    <div class="form-group col-sm-6">
       
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
            <?= Html::resetButton('Vaciar',['class' => 'btn btn btn-success'])?>
        
    </div>

    <?php ActiveForm::end(); ?>
   
</div>





