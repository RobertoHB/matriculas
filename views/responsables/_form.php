<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Responsables */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="responsables-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_tutor')->textInput() ?>

    <?= $form->field($model, 'dni_alumno')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
