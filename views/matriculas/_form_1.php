<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use app\models\Alumnos;
use app\models\Ciclos;
use app\models\Datosbancarios;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Matriculas */
/* @var $form yii\widgets\ActiveForm */

 //$model->dni_alumno = $_GET['dni_alumno']; 



$abonaSeguro = [0 => 'No', 1 => 'Si'];
$cursosAcademicos = ['18-19','19-20','20-21','21-22','22-23','23-24','24-25','25-26','26-27','27-28','28-29','29-30'
                    ,'30-31','31-32','32-33','33-34','34-35','35-36','36-37','37-38','38-39','39-40','40-41','41-42'
                    ,'42-43','43-44','44-45','45-46','46-47','47-48','48-49','49-50'];
$tipoMatricula = ['Ordinaria','Modular'];
$itemAlumnos = ArrayHelper::map(Alumnos::find()->all(), 'dni', 'nombre','apellidos');
$itemCiclos = ArrayHelper::map(Ciclos::find()->all(), 'id', 'denominacion');
$itemBancos = ArrayHelper::map(Datosbancarios::find()->all(), 'id', 'iban','titular','dni');    
$itemCursos = [1 => 'Primero', 2 => 'Segundo'];
$itemCuentas = ArrayHelper::map(Datosbancarios::find()->all(), 'id','iban');



if (array_key_exists($model->id, $itemCuentas)) {
    echo("cuenta bancaria existe");
}

?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

  <h3>Alumno</h3>
    
 <?= GridView::widget([
        'dataProvider' =>$datosAlumno,
        'summary' => '',
        //'filterModel' => $searchModel,
        'columns' => [
            'dni',
            'nombre',
            'apellidos',
        ],
    ]); ?>

<div class="matriculas-form">

    <?php $form = ActiveForm::begin(); ?>
    
      
      
    <div class="form-group row">
        <div class="col-sm-4">
     <?= $form->field($model, 'fecha')->widget(DatePicker::className(), [
          // inline too, not bad
          'inline' => false, 
           // modify template for custom rendering
          //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
          'clientOptions' => [
          'autoclose' => true,
          'format' => 'dd-mm-yyyy',
          'todayBtn' => true
          ]
    ]);?>
        </div>
         <div class="col-sm-4">
     <?= $form->field($model, 'curso_academico')->dropDownList($cursosAcademicos, ['prompt' => 'Seleccione Uno' ]); ?>
         </div>
        <div class="col-sm-4">
    
    <?= $form->field($model, 'tipo')->dropDownList($tipoMatricula, ['prompt' => 'Seleccione Uno' ]); ?>
        </div>
    </div> 
     <div class="form-group row">
        <div class="col-sm-8">
            <?= $form->field($model, 'id_ciclo') ->dropDownList($itemCiclos, // Flat array ('id'=>'label')
                                                    ['prompt'=>'']);  ?>   
        </div>
        <div class="col-sm-4">
     <?= $form->field($model, 'curso') ->dropDownList($itemCursos, // Flat array ('id'=>'label')
                                                    ['prompt'=>'']);  ?>   
        </div>    
     </div>
     <div class="form-group row">
        <div class="col-sm-6">
            <?= $form->field($model, 'id_datos_bancarios') ->dropDownList($itemBancos, // Flat array ('id'=>'label')
                                                    ['prompt'=>'']);  ?>   
        </div>
         <div class="col-sm-2">
               <?= Html::label('Nº Iban',$options=['class'=>'form-control']) ?>
               <?= Html::input('text','iban','', $options=['class'=>'form-control','style'=>'height:40px']) ?>
         </div>
         <div class="col-sm-2">
              <?= Html::label('Titular',$options=['class'=>'form-control']) ?>
            <?= Html::input('text','titular','', $options=['class'=>'form-control','style'=>'height:40px']) ?>
         </div>
         <div class="col-sm-2">
              <?= Html::label('Dni',$options=['class'=>'form-control']) ?>
            <?= Html::input('text','dni','', $options=['class'=>'form-control','style'=>'height:40px']) ?>
         </div>
     </div>
   <div class="form-group row">
       <div class="form-group" style="border:1px solid grey;border-radius: 5px;height: 25px;width:800px;margin:1px;height: 30px;">
                <div class="col-sm-2">
        <!--                Abona Seguro<a class="btn btn-xs btn-success" href="#" style="margin-left: 10px;"><i class="fa fa-check fa-1x"></i></a>-->
                    <?= $form->field($model, 'abona_matricula')->checkbox(['label' => 'Seguro', 'class'=>'form-check-input'])->label('') ?>
                </div>    
                <div class="col-sm-2">
        <!--                 Firma Matricula<a class="btn btn-xs btn-success" href="#" style="margin-left: 10px;"><i class="fa fa-check fa-1x"></i></a>-->
                     <?= $form->field($model, 'f_matricula')->checkbox(['label' => 'Firma', 'class'=>'form-check-input'])->label('') ?>
                </div>
                <div class="col-sm-2">
        <!--             Protección Datos<a class="btn btn-xs btn-success" href="#" style="margin-left: 10px;"><i class="fa fa-check fa-1x"></i></a>-->
                  <?= $form->field($model, 'f_protedatos')->checkbox(['label' => 'Protección', 'class'=>'form-check-input'])->label('') ?>
                </div>
                
        </div>   
       
           
        
   </div>
  <div class="form-group row"> 
         <div class="col-sm-8">
                    <picture>
                      <?= Html::img('@web/img/firma-electronica.png', ['alt' => 'firma electronica', 'style' => 'width:70px;border-radius:15px;']); ?>
                    </picture>
            </div>

  </div>
    <div class="form-group row">
          <div class="col-sm-12">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
          </div>
    </div>

    <?php ActiveForm::end(); ?>
  
</div>
  
  <div class="grid_modulos">
      
        <h3>Módulos</h3>
    
 <?= GridView::widget([
        'dataProvider' =>$datosModulo,
        'summary' => '',
        //'filterModel' => $searchModel,
        'columns' => [
            'id',
            'id_matricula',
            'id_modulo',
            'estado',
        ],
    ]); ?>
      
  </div>
