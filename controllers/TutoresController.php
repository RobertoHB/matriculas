<?php

namespace app\controllers;

use Yii;
use app\models\Tutores;
use app\models\TutoresSearch;
use app\models\Responsables;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\base\Application;


/**
 * TutoresController implements the CRUD actions for Tutores model.
 */
class TutoresController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tutores models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TutoresSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tutores model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tutores model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tutores();
            
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
       
           return $this->redirect(['view', 'id' => $model->id]);
            
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Tutores model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
       $alumno = $_GET['alumno'];
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
           //hacer un history -2 cuando se guarde el registro
           return $this->redirect(['responsables/index','alumno'=>$alumno]);
           //return $this->redirect(['update', 'id' => $model->id,]);//actualizado
            
       
        }

        return $this->render('update', [
            'model' => $model,
           
        ]);//no actualizado
    }

    /**
     * Deletes an existing Tutores model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tutores model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tutores the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tutores::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    public function actionDatostutor($alumno){
       // $modelDatosTutor = new Tutores();
        $tutor = $_REQUEST['Tutores']['lista_tutores'];
        if (isset($alumno)&& isset($tutor)){
            return $this->redirect(['guardar_responsable_alumno', 'alumno' => $alumno,'tutor' => $tutor]);
        }
      
    }
    
    
    public function actionGuardartutor($alumno){
        
        $nombreTutor= $_POST['Tutores']['nombre'];
        $apellidosTutor= $_POST['Tutores']['apellidos'];  
        $modelTutores = new Tutores();
      
        if($modelTutores->tutorDuplicado($nombreTutor, $apellidosTutor)){
               // throw new NotFoundHttpException('Tutor duplicado o datos incorrectos');
            //Yii::$app->user->setFlash('success',"Datos duplicados para el Tutor introducido");
           
           //echo json_encode(array('error' => 'Posible registro duplicado'));
            
             //$modelTutores->addError('apellidos', 'Tutor incorrecto o ya existe');
            
        
             return $this->redirect(['responsables/index','alumno'=>$alumno]);
              
        }else{
            $modelTutores->save();
        }            
        //$modelTutores->save();
        
        if ($modelTutores->load(Yii::$app->request->post()) && $modelTutores->save()) {
            return $this->redirect(['guardar_responsable_alumno', 'alumno' => $alumno,'tutor' => $modelTutores->id]);
        }
        
    }
    
    public function actionGuardar_responsable_alumno($alumno,$tutor){
        $modelResponsableAlumno = new Responsables();
        if (isset($alumno) && isset($tutor)){
            $modelResponsableAlumno->id_tutor = $tutor;
            $modelResponsableAlumno->dni_alumno = $alumno;
            $modelResponsableAlumno->save();
            return $this->redirect(['responsables/index','alumno'=>$alumno]);
            
        }
        
        
        
    }
}
