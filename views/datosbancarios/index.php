<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DatosbancariosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Datosbancarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="datosbancarios-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Datosbancarios', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'iban',
            'titular',
            'dni',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
