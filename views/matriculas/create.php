<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Alumnos;

/* @var $this yii\web\View */
/* @var $model app\models\Matriculas */

$this->title = 'Create Matriculas';
$this->params['breadcrumbs'][] = ['label' => 'Matriculas', 'url' => ['index','alumno' => $_REQUEST['alumno']]];
$this->params['breadcrumbs'][] = $this->title;
if(!isset($msg))$msg=''
?>
<div class="matriculas-create">

    <?= $this->render('_form', [
        'model' => $model,
        'datosAlumno'=>$datosAlumno,
        'datosModulo' => $datosModulo,
        'modelModulos'=>$modeloModulos,
        'modelDatosBancarios' => $modeloDatosBancarios,
       
    ]) ?>

</div>
