<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use app\models\Alumnos;
use app\models\Ciclos;
use app\models\Modulos;
use app\models\Modulosciclo;
use app\models\Datosbancarios;
use app\models\Bancos;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $model app\models\Matriculas */
/* @var $form yii\widgets\ActiveForm */

 //$model->dni_alumno = $_GET['dni_alumno']; 


//if (isset($_REQUEST[1]['alumno'])){
//   $alumno = $_REQUEST[1]['alumno'];
// 
//}else{
    $alumno = $_REQUEST['alumno'];
//}

if (!isset($_REQUEST['id'])){
    $matricula = 0;
}else{
    $matricula = $_REQUEST['id'];
}   

$abonaSeguro = [0 => 'No', 1 => 'Si'];
$cursosAcademicos = ['18-19'=>'18-19','19-20'=>'19-20','20-21'=> '20-21','21-22' => '21-22','22-23' => '22-23','23-24'=>'23-24',
                    '24-25'=>'24-25','25-26'=>'25-26','26-27'=>'26-27','27-28'=>'27-28','28-29'=>'28-29','29-30'=>'29-30',
                    '30-31'=>'30-31', '31-32'=>'31-32', '32-33'=>'32-33', '33-34'=>'33-34', '34-35'=>'34-35', '35-36'=>'35-36',
                    '36-37'=>'36-37', '37-38'=>'37-38', '38-39'=>'38-39', '39-40'=>'39-40', '40-41'=>'40-41', '41-42'=>'41-42',
                    '42-43'=>'42-43','43-44'=>'43-44','44-45'=>'44-45','45-46'=>'45-46','46-47'=>'46-47','47-48'=>'47-48','48-49'=>'48-49',
                    '49-50'=>'49-50'];

//var_dump(ArrayHelper::map($cursosAcademicos,'nombre','id'));
//exit;
//print_r(ArrayHelper::getValue($cursosAcademicos,5));
//exit;
$tipoMatricula = ['Ordinaria','Modular'];
$itemAlumnos = ArrayHelper::map(Alumnos::find()->all(), 'dni', 'nombre','apellidos');
$itemCiclos = ArrayHelper::map(Ciclos::find()->all(), 'id', 'denominacion');
$itemModulos = ArrayHelper::map(Modulos::find()->all(), 'id', 'nombre');
$itemBancos = ArrayHelper::map(Datosbancarios::find()->all(), 'id', 'titular','iban','dni');    
$itemCursos = [1 => 'Primero', 2 => 'Segundo'];
$itemCuentas = ArrayHelper::map(Datosbancarios::find()->all(), 'id','iban');
$itemNombreBanco = ArrayHelper::map(Bancos::find()->all(), 'nombre','nombre');




if (array_key_exists($model->id, $itemCuentas)) {
    echo("cuenta bancaria existe");
}


?>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="<?= Url::to('@web/css/firma.css')?>">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!--<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>-->
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>

  <script src="<?= Url::to('@web/js/firma_electronica.js')?>"></script>


  <h3>Alumno</h3>
    
 <?= GridView::widget([
        'dataProvider' =>$datosAlumno,
        'summary' => '',
        //'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute'=>'passnie',
                'contentOptions' => ['style' => 'width:10px; white-space: normal;text-align:center;'],
            ],
         
            'dni',
            'nombre',
            'apellidos',
        ],
    ]); ?>

<div class="matriculas-form">
    <div class ="col col-sm-12 row" hidden> 
        <?php $form = ActiveForm::begin(['id'=>'form_matriculas']); ?>

         <?= ($alumno > 0) ? $form->field($model, 'dni_alumno')->hiddenInput(['value'=>$alumno,'id'=>'alumno_dni'])->label(''):
            $form->field($model, 'dni_alumno')->hiddenInput(['value'=>$model->dni_alumno,'id'=>'alumno_dni'])->label('');?>

        <?= $form->field($model, 'id')->hiddenInput()->label(''); ?>
    </div>    
    
    <div class ="col col-sm-6" style="margin-top: 30px;"> 
            <div class="form-group row">
                <div class="col-sm-4">
             <?= $form->field($model, 'fecha')->widget(DatePicker::className(), [
                  // inline too, not bad
                  'inline' => false, 
                   // modify template for custom rendering
                  //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
                  'clientOptions' => [
                  'autoclose' => true,
                  'format' => 'dd-mm-yyyy',
                  'todayBtn' => true
                  ]
            ]);?>
                </div>
                 <div class="col-sm-4">
             <?= $form->field($model, 'curso_academico')->dropDownList($cursosAcademicos, ['prompt' => '']); ?>
                 </div>
                <div class="col-sm-4">

            <?= $form->field($model, 'tipo')->dropDownList($tipoMatricula, ['prompt' => '','id' => 'tipo_matricula','class'=>'form-control listaModulos' ]); ?>
                </div>

            </div>
            <div class="form-group row">
                <div class="col-sm-8">
                    <?= $form->field($model, 'id_ciclo') ->dropDownList($itemCiclos, // Flat array ('id'=>'label')
                                                            ['prompt'=>'','id' => 'ciclo_matricula','class'=>'form-control listaModulos' ]);  ?>   
                </div>
                <div class="col-sm-4">
             <?= $form->field($model, 'curso') ->dropDownList($itemCursos, // Flat array ('id'=>'label')
                                                            ['prompt'=>'','id' => 'curso_matricula','class'=>'form-control listaModulos' ]);  ?>   
                </div>    
            </div>
        
         <?= $form->field($model, 'id_datos_bancarios')->hiddenInput(['prompt'=>'','id' => 'datos_banco'])->label(''); ?> 
        
        <div class="form-group row" style="margin-top: -30px;margin-bottom: 10px;">
             <div class="col-sm-12">
                <span STYLE="color:#337ab7;"><b>DATOS BANCARIOS</b></span>
                <!--formulario que guardará los datos bancarios y asignará el id_datos_bancarios a la matricula-->
                <button type="button" class="btn btn-info btn-group-sm" id="linkdatosBancos">
                    <span class="glyphicon glyphicon-plus" id="abrir_form_datos_bancarios"></span>  
                </button>
            </div>    
                
     
         </div>    
           
            <div class="form-group row" id="datos_bancarios_txt" style="margin-bottom: 25px;">
                
                <div class="col-sm-6">
                    <?= Html::label('Entidad',$options=['class'=>'form-control']) ?>
                    <?= Html::input('text','entidad','', $options=['class'=>'form-control','style'=>'height:40px','id'=>'entidad_db']) ?>
                </div>
                <div class="col-sm-6">
                    <?= Html::label('Cuenta',$options=['class'=>'form-control']) ?>
                    <?= Html::input('text','cuenta','', $options=['class'=>'form-control','style'=>'height:40px','id'=>'cuenta_db']) ?>
                </div>
                 <div class="col-sm-6">
                    <?= Html::label('Titular',$options=['class'=>'form-control']) ?>
                    <?= Html::input('text','titular','', $options=['class'=>'form-control','style'=>'height:40px','id'=>'titular_db']) ?>
                </div>
                <div class="col-sm-6">
                    <?= Html::label('Dni',$options=['class'=>'form-control']) ?>
                    <?= Html::input('text','dni','', $options=['class'=>'form-control','style'=>'height:40px','id'=>'dni_db']) ?>
                </div>
            </div>
        
        
        <div class="form-group row">
            <div class="form-group" style="border:1px solid grey;border-radius: 5px;height: 25px;width:550px;margin:10px;height: 28px;">
                     <div class="col-sm-3">
             <!--                Abona Seguro<a class="btn btn-xs btn-success" href="#" style="margin-left: 10px;"><i class="fa fa-check fa-1x"></i></a>-->
                         <?= $form->field($model, 'seguro')->checkbox(['label' => 'Seguro', 'class'=>'form-check-input','id'=>'abona_seguro'])->label('') ?>
                     </div>    
                     <div class="col-sm-3">
             <!--                 Firma Matricula<a class="btn btn-xs btn-success" href="#" style="margin-left: 10px;"><i class="fa fa-check fa-1x"></i></a>-->
                          <?= $form->field($model, 'f_matricula')->checkbox(['label' => 'Firma', 'class'=>'form-check-input','id' => 'f_matricula'])->label('') ?>
                          <?= $form->field($model, 'firma')->textInput(['maxlength' => true, 'id' => 'firma_matricula','style'=>"display:none"])->label(false); ?>
                        
                     </div>
                <div class="col-sm-3">
                    <?= $form->field($model, 'privez')->checkbox(['label' => '1ªVez', 'class'=>'form-check-input','id' => 'privez'])->label('') ?>
                      <!--Html::input('checkbox','privez','0', $options=['class'=>'form-check-input']);--> 
                </div>
                <div class="col-sm-3">
                    <?= $form->field($model, 'repite')->checkbox(['label' => 'Repite', 'class'=>'form-check-input','id' => 'repite'])->label('') ?>
                      <!--Html::input('checkbox','repite','0', $options=['class'=>'form-check-input']);--> 
                </div>
                   
                      
                  


             </div>   

        </div>
       
       <div class="form-group row">
            <!--<div class="col-sm-2">
                 <!--Html::submitButton('Save', ['class' => 'btn btn-success'])-->
                 
                 <!--Html::img('@web/img/firma-electronica.png', ['alt' => 'firma electronica', 'style' => 'width:70px;border-radius:15px;']);--> 
              
             
                
            <!--</div>-->
        
           
           <div class="col-sm-3">
                 <!--Html::submitButton('Grabar', ['class' => 'btn btn-success']);-->
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Guardar') : Yii::t('app', 'Actualizar'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','id'=>'envioForm']) ?>
           </div>
           <div class="col-sm-3">
<!--                Html::a('Imprimir', ['matriculas/generarpdf','matricula'=>$model->id,'tipo'=>'V'],['class' => 'btn btn-success','id' => 'modalImpresos']);-->
                <?= Html::button('Imprimir', ['class' => 'btn btn-success','id' => 'modalImpresos']);?>
           </div>
             <div class="col-sm-3">
                 <?= Html::Button('Firma Digital', ['class' => 'btn btn-success','id'=>'bot_modal_firma']);?>
           </div>
            
         
       </div>

           <?php ActiveForm::end(); ?>
  
    </div>
    
     <div class="form-group row" id="formulario_datos_bancarios" style="background-color: #DFEEF7;border-radius: 15px;padding: 5px;position:absolute;top:520px;left:275px;width: 575px;height:190px; ">
        <div class="col-sm-12" >
           
            <?php $form_datos_bancarios = ActiveForm::begin([
                'method' => 'post',
                'id'=>'form_datosbancarios',
                'enableClientValidation' => true,
                'enableAjaxValidation' => true,
               // 'action'=> ['/datosbancarios/guardardatosbancarios'],
               
                
                ]); ?>

            <div class="col-sm-6">
                <?= $form_datos_bancarios->field($modelDatosBancarios, 'banco')->dropDownList($itemNombreBanco, ['prompt' => '']); ?>
            </div>
            <div class="col-sm-6">
                <?= $form_datos_bancarios->field($modelDatosBancarios, 'iban')->textInput()->label('Iban'); ?>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-6">
                <?= $form_datos_bancarios->field($modelDatosBancarios, 'titular')->textInput()->label('Titular'); ?>
            </div>
            <div class="col-sm-6">
                <?= $form_datos_bancarios->field($modelDatosBancarios, 'dni')->textInput()->label('Dni'); ?>
            </div>
        </div>   
        
         <div class="col-sm-12" style="padding-left: 15px;">         
            <!--<i class="fas fa-check-circle"></i>-->
            
            <!--Html::a('',['class' => 'fas fa-check-circle','style' => 'margin-left:18px','id'=>'envio_form_datos_bancarios']);-->
           <!--Html::a('', ['guardar', 'id' => 'envio_form_datos_bancarios'], ['class' => 'fas fa-check-circle fa-2x','title'=>'Guardar','style' =>'text-decoration:none'])-->
           
            <div class="col-sm-1"> 
                <?=Html::Button("<span class='fas fa-check-circle fa-2x' aria-hidden='true' style='background-color:transparent'></span>", ['style' => 'margin-left:3px;background-color:transparent;border:none','id' => 'envio_form_datos_bancarios','title'=>'guardar'])?>
            </div>
            <div class="col-sm-1"> 
                <?= Html::Button("<span class='fas fa-eraser fa-2x' aria-hidden='true'></span>", ['id' => 'limpiar_form_bancos','title'=>'vaciar','type'=>'reset','style'=>'background-color:transparent;border:none']) ?>
                <?php ActiveForm::end();?>                
            </div>
           <div class="col-sm-10" style="text-align: center;"> 
            <span id="txt_guardado_db" stytle=""></span>
               
            </div>
        
        </div>        
     </div>
                
    
    
    
    
    
    
    
    
    <div class="col col-sm-6">
        <div class="form-group row">
            <div class="col-sm-12">
                <h3>Modulos</h3>
                <?php //Pjax::begin(['id' => 'modulosMatricula']) ?>
                    <?= GridView::widget([
                           'dataProvider' =>$datosModulo,
                           'summary' => '',
                           //'filterModel' => $searchModel,
                           'columns' => [
                              // 'id',
                              // 'id_matricula',
                              // 'id_modulo',
                              // 'estado',
                               
                           
                           [
                            'label' => 'Modulo',
                            'attribute' => 'id_modulo',
                            'value' => 'modulo0.nombre',
                            'enableSorting' => true,
                          ],
                             'estado',          
                           
                          ['class' => 'yii\grid\ActionColumn',
                
                            'contentOptions' => ['style' => 'width:80px;'],
                            'header'=>'',
                            'template' => '{update}{delete}',
                            'buttons' => [
                                            'update' => function ($url,$model,$key) {
                                                return Html::button(
                                                '<span class="glyphicon glyphicon-pencil"></span>',['value' =>url::to('@web/modulosmatricula/update'.'?id='.$model->id),'id'=>'modalButton','style'=>'border:none;background-color:transparent;']);
                                                       
//                                                    ['data'=>[
//                                                                'method' => 'get',
//                                                                'params'=>['id'=>$model->id],                                         
//                                                                ]
//                                                    ]
//                                                        );
                                             },
                                            'delete' => function ($url, $model, $key) {
                                                return  Html::a(
                                                '<span class="glyphicon glyphicon-trash" style="padding-left:5px;"></span>',Url::to('@web/modulosmatricula/delete'.'?id='.$model->id),                                                                
                                                         ['data'=>[
                                                                'method' => 'post',
                                                                'confirm' => 'Are you sure?',
                                                                'params'=>['alumno'=>$_REQUEST['alumno'],'matricula'=>$_REQUEST['id']],
                                                                ]
                                                        ]);
                                            }
                                        ]
                            ],       
                                   ],        
                       ]); ?>
                <?php//Pjax::end() ?>

            </div>
        </div> 
        <div class="form-group row">
            <div class="col-sm-12">
                <?php //yii\widgets\Pjax::begin(['id' => 'form_add_modulo']) ?>
                    <?php $form = ActiveForm::begin([
                         'method' => 'get',
                         'action' => ['/modulosmatricula/anadirmodulo','matricula'=>$matricula,'alumno'=>$model->dni_alumno],
                        'options' => ['data-pjax' => true ]]);?> 
                
                         <?=$form->field($modelModulos, 'id') ->dropDownList($itemModulos,
                                    [
                                        'prompt'=>'Añadir un Modulo',
                                        'id'=> 'lista_modelos'
                                                                          
                                    ]
                                )->label('Modulos');?>
                  
             </div>   
            <div class="col-sm-12">         
                      <?=Html::submitButton('Añadir', ['class' => 'btn btn-success']);?>
 
                    <?php ActiveForm::end();?>
                <?php //ii\widgets\Pjax::end() ?>
            </div>
        
               
     </div>
        
   
                   
        
        
        
        
        
        
        
        <!--div que contiene el canvas para realizar la firma.--> 
   

        <!--ventana modal para actualizar modulos-->
        <div id="modal_actualizar_modulo">
               <?php Modal::begin([
                        'id'=>'modal_act_modulos',
                        'size'=>'modal-lg',
                        'header' => '<h2>Actualizacion de Modulos </h2>',
                            //'toggleButton' => ['label' => 'click me'],
                    ]);

                       echo "<div id='modalContent'></div>";

                 Modal::end();?>


        </div>
         <!--ventana modal para crear datos bancarios-->
        <div id="modal_crear_cuentas">
               <?php Modal::begin([
                        'id'=>'modal_cuentas',
                        'size'=>'modal-lg',
                        'header' => '<h2>Datos Bancarios </h2>',
                        
                            //'toggleButton' => ['label' => 'click me'],
                    ]);

                       echo "<div id='modalBancos'></div>";

                 Modal::end();?>


        </div>
          <!--ventana modal para crear impresos pdf-->
        <div id="modal_crear_impresos">
               <?php Modal::begin([
                        'id'=>'modal_impresos',
                        'size'=>'modal-xs',
                        'header' => '<h2>Generar Impresos </h2>',
                        
                            //'toggleButton' => ['label' => 'click me'],
                    ]);?>
            <div class="row form-group" style="padding:20px">
                   <!--Html::button( Html::a('Solicitud', ['matriculas/generarpdf','matricula'=>$model->id,'tipo'=>'V'],['class' => 'fa fa-file-alt fa-2x','target'=>'_blank']));?>-->
                   <?=Html::button( Html::a('Matricula', ['site/pdfmatricula','matricula'=>$model->id,'tipo'=>'V'],['target'=>'_blank']),['class' => 'btn btn-warning']);?>
                   <?=Html::button( Html::a('Recibo', ['site/pdfrecibo','alumno'=>$model->dni_alumno,'fechamat'=>$model->fecha],['target'=>'_blank']),['class' => 'btn btn-success']);?>
                   <?=Html::button( Html::a('Cuenta', ['site/pdfdatosbanco','alumno'=>$model->dni_alumno,'codigobanco'=>$model->id_datos_bancarios],['target'=>'_blank']),['class' => 'btn btn-danger']);?>
                   <?=Html::button( Html::a('Autorizacion', ['site/pdfautorizacion','matricula'=>$model->id,'tipo'=>'V'],['target'=>'_blank']),['class' => 'btn btn-info']);?>
                   <?=Html::button( Html::a('Carpeta', ['site/pdfcarpeta','matricula'=>$model->id,'tipo'=>'V'],['target'=>'_blank']),['class' => 'btn btn-light']);?>
                   <?=Html::button( Html::a('Portada', ['site/pdfportada','matricula'=>$model->id,'tipo'=>'V'],['target'=>'_blank']),['class' => 'btn btn-warning']);?>                                      
            </div>    
                      

                <?php Modal::end();?>


        </div>

        
        
</div>  
   
    <div id="contenedorCanvas" class="container-fluid" width="100%" hidden>
      <span id="cerrar_Ventana_firma" style="text-align: right;"><i class="far fa-times-circle fa-2x" id="cerrar_firma"></i><span>
      <h3 id="titulofirma">Firma Matrícula</h3>
      <canvas width="300" height="200" style="border: none;border-radius:5%">
      
      </canvas>

      <div id="contenedor_imag" class="container-fluid" hidden>
      </div>  

      <div id="botones">
           <button class="canva" id="borrar"><i class="fas fa-times fa-2x"></i></button>
           <button class="canva" id="validar"><i class="fas fa-check fa-2x"></i></button>
           <button class="canva" id="confirmar"><i class="fas fa-share fa-2x"></i></button>
       </div>

    </div>
  
        
  
<script>
 $( document ).ready(function() {
     
       //hacemos toogle sobre el formulario de introducción de datos bancarios para que no aparezca si no se hace click en +
       
       $('#formulario_datos_bancarios').toggle();
     
       //controlamos si se selecciona algun otro valor en cualquier campo con la clase listaModulos para actualizar el buscador de modulos. El campo del que
       //controlamos el cambio en este caso es el de curso.
       $('.listaModulos').change(function(event) {
            actualizarRepitePrimera();
            //si los objetos de la clase .listamodulos no son null
          
            if($('#ciclo_matricula').val() != '' && $('#tipo_matricula').val() != ''){
                    //console.log($('#tipo_matricula').val());
                        actualizarModulos(); 
                        
            }        
        
    });
    
    
    //al enviar formulario de nuevos datos bancarios
    //$(document).on("click", "#envio_form_datos_bancarios", function () {
    $('#envio_form_datos_bancarios').on('click', function(e) {
        
        $ .ajax ({
         url: "../datosbancarios/guardardatosbancarios",
         method: "POST",
         data: $ (this.form) .serialize (),
        success: function (data) {
         
          
//            si la cuenta bancaria ya existe no se guardara y el mensaje sera de cuenta ya existe
//            if(data == 0){
//              
//                $("#txt_guardado_db").text("La cuenta ya existe");
//                
//                for(let i=0;i<10;i++){
//                     $("#txt_guardado_db").fadeToggle(500);
//                    
//                 }    
//                
//            }else{    
                $('#datos_banco').val(data);
                actualizarDatosBancarios(); 
                $("#txt_guardado_db").text("Guardado");
                for(let i=0;i<5;i++){
                     $("#txt_guardado_db").fadeToggle(500);    
                 }   
                //$('#linkdatosBancos').click(); 
               


        },

        error: function () {
 
            alert("Error");

        }
    });
    });
    
   
 
    
//       $( document ).ready(function(event) {
//           actualizarModulos(); 
//        }); 
        
//       $( document ).ready(function(e) {
//            if($('#tipo_matricula')!= null){
//                actualizarModulos(); 
//            }    
//si el boton de submit tiene la clase succes es nueva matricula y actualiza el check de seguro y si es un update actualiza el buscador de modulos
            if($('#envioForm').hasClass("btn btn-primary")){
                actualizarModulos(); 
                actualizarDatosBancarios();
            }     
           if($('#envioForm').hasClass("btn btn-success")){
                abonaSeguro(); 
            }     
//        }); 
        
        
            
   


    
    //controlamos cualquier nueva seleccion de datos en el campo con id datos_banco para que nos devuelva el nombre del titular de cta y su dni
     $('#datos_banco').change(function(){
         actualizarDatosBancarios();
             
    });
    //controlamos el click en actualizacion de modulos para mostrar la ventana modal con el formulario de modulosmatricula
 $(document).on("click", "#modalButton", function () {
        //console.log("mostrar modal")
         $('#modal_act_modulos').modal('show')
            .find('#modalContent')
            .load($(this).attr('value'));
    });
       
 $(document).on("click", "#linkdatosBancos", function () {
      
           $('#formulario_datos_bancarios').slideToggle( "slow" ); 
         
         
           if( $('#abrir_form_datos_bancarios').hasClass('glyphicon glyphicon-plus')){
                $('#abrir_form_datos_bancarios').removeClass('glyphicon glyphicon-plus');     
                $('#abrir_form_datos_bancarios').addClass('glyphicon glyphicon-minus');
                 $('#datos_bancarios_txt').animate({ marginTop: '210px'}, 1000);
           }else{
                $('#abrir_form_datos_bancarios').removeClass('glyphicon glyphicon-minus');     
                $('#abrir_form_datos_bancarios').addClass('glyphicon glyphicon-plus'); 
                $('#datos_bancarios_txt').animate({ marginTop: '-210px'}, 1000);
           }    
                 
             
//         $('#modal_cuentas').modal('show')
//            .find('#modalBancos')
//            .load($(this).attr('value'));
    });
    
    $(document).on("click", "#modalImpresos", function () {

         $('#modal_impresos').modal('show')
    //        .find('#modalBancos')
    //        .load($(this).attr('value'));
    });

 });
    function actualizarDatosBancarios(){
        var baseUrl = "<?= Url::toRoute(["/"]); ?>"; 
            $.ajax({

                type: "GET",
                dataType: "json",
              
                url: baseUrl + "datosbancarios/datosbanco",

                data: {

                    id: $("#datos_banco").val()
                   
                }

            })
                 .done(function (data) {
                    
                   var pais = data['iban'].substr(0,4);
                   var entidad = data['iban'].substr(4,4);
                   var oficina = data['iban'].substr(8,4);
                   var dc = data['iban'].substr(12,2);
                   var cuenta = data['iban'].substr(14,10);
           
                   //console.log(pais,entidad,oficina,dc,cuenta);
                $( "#entidad_db" ).val(data['entidad']);
                $( "#cuenta_db" ).val( pais + "-" + entidad + "-" + oficina + "-" + dc + "-" + cuenta );
                $( "#dni_db" ).val( data['dni'] );
                 $( "#titular_db" ).val( data['titular'] );
                
            }); 
    
    }
    function actualizarModulos(){    
        var baseUrl = "<?= Url::toRoute(["/"]); ?>"; 
          $.ajax({

              type: "GET",
              url: baseUrl + "modulosciclo/anadirmodulo",

              data: {

                  tipo: $("#tipo_matricula").val(),
//                  curso: $("#curso_matricula").val(),
                  ciclo: $("#ciclo_matricula").val()

              }

          })
               .done(function (data) {
               $( "#lista_modelos" ).html( data );
          });
    }     
        
    function  actualizarRepitePrimera(){
        var baseUrl = "<?= Url::toRoute(["/"]); ?>"; 
        $.ajax({

           type: "GET",
           url: baseUrl + "matriculas/repiteprimera",

           data: {
               alumno: $("#alumno_dni").val(),
               curso: $("#curso_matricula").val(),
               ciclo: $("#ciclo_matricula").val()

           }

        })
            .done(function (data) {
                var obj = $.parseJSON(data);
                var repite = obj['repite'];
                var primera = obj['primera'];
                
//                console.log('repite:'+repite+' '+'Primera:'+primera);
//                if (repite == 1) $( "#repite" ).prop('checked', true)
                var resultado_repite = (repite == 0) ? $( "#repite" ).prop('checked', false):  $( "#repite" ).prop('checked', true);
                var resultado_primera = (primera == 0) ? $( "#privez" ).prop('checked', false): $( "#privez" ).prop('checked', true);
                // var x = (day == "yes") ? "Good Day!" : (day == "no") ? "Good Night!" : "";
//                if (primera == 1) $( "#primera" ).prop('checked', true)
               
              
            });
            
    }    
            
    function abonaSeguro(){    

      var baseUrl = "<?= Url::toRoute(["/"]); ?>"; 
        $.ajax({

            type: "GET",
            url: baseUrl + "matriculas/seguro",

            data: {
                    alumno: $("#alumno_dni").val()

            }

        })
             .done(function (data) {
            if(data == 1){      
                $("#abona_seguro").prop('checked', true);
            }
        });
    }         
      
</script>