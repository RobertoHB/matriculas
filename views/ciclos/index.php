<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CiclosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ciclos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ciclos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Ciclo', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute'=>'id',
                'contentOptions'=>['style'=>'width:80px']    
            ],
            [
                'attribute'=>'referencia',
                'contentOptions'=>['style'=>'width:100px']    
            ],    
            'denominacion',
            'reflegal1',
            'reflegal2',

//            ['class' => 'yii\grid\ActionColumn'],
            ['class' => 'yii\grid\ActionColumn',
                
                'contentOptions' => ['style' => 'width:80px;'],
                'header'=>'',
                'template' => '{actualizar}',
                'buttons' => [
                   'actualizar' => function ($url, $model, $key) {
                                                return  Html::a(
                                                '<span class="glyphicon glyphicon-pencil" style="padding-left:5px;"></span>',Url::to('@web/ciclos/update'.'?id='.$model->id)                                                                
                                                        
                                                        );
                                },
                            ]
            
            ],
        ],
    ]); ?>


</div>
