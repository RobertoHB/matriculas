<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AlumnosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="alumnos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_escolar') ?>

    <?= $form->field($model, 'expe_centro') ?>
    
    <?= $form->field($model, 'passnie') ?>
    
    <?= $form->field($model, 'dni') ?>

    <?= $form->field($model, 'nombre') ?>

    <?= $form->field($model, 'apellidos') ?>

    <?php // echo $form->field($model, 'f_nac') ?>

    <?php // echo $form->field($model, 'loc_nac') ?>

    <?php // echo $form->field($model, 'prov_nac') ?>

    <?php // echo $form->field($model, 'domicilio') ?>

    <?php // echo $form->field($model, 'cp') ?>

    <?php // echo $form->field($model, 'localidad') ?>

    <?php // echo $form->field($model, 'provincia') ?>

    <?php // echo $form->field($model, 'tel_fijo') ?>

    <?php // echo $form->field($model, 'movil') ?>

    <?php // echo $form->field($model, 'discapacidad') ?>

    <?php // echo $form->field($model, 'centro_ant') ?>

    <?php // echo $form->field($model, 'tit_acceso') ?>

    <?php // echo $form->field($model, 'foto') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'nacionalidad') ?>

  

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
