<?php

namespace app\controllers;

use Yii;
use app\models\Modulosmatricula;
use app\models\ModulosmatriculaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ModulosmatriculaController implements the CRUD actions for Modulosmatricula model.
 */
class ModulosmatriculaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Modulosmatricula models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ModulosmatriculaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Modulosmatricula model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Modulosmatricula model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Modulosmatricula();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Modulosmatricula model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
           return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        }

        return $this->renderPartial('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Modulosmatricula model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $alumno = $_REQUEST['alumno'];
        $matricula = $_REQUEST['matricula'];
        $this->findModel($id)->delete();

        return $this->redirect(['/matriculas/update','alumno'=>$alumno,'id'=>$matricula]);
    }

    
    public function actionAnadirmodulo($matricula,$alumno){
      
       $model = new Modulosmatricula();
       $modulo = $_REQUEST['Modulos']['id'];
       $model->id_matricula = $matricula;
       $model->id_modulo = $modulo;
       $model->estado = 'NA';
       $model->save();
    if ($model->load(Yii::$app->request->get())) {
            if ($model->save()) {
               echo 'Registro guardado';

            }
    } else {
          
             return $this->redirect(['/matriculas/update','alumno'=>$alumno,'id'=>$matricula]);
        }
        return $this->redirect(['/matriculas/update','alumno'=>$alumno,'id'=>$matricula]);
    }
    
    
    /**
     * Finds the Modulosmatricula model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Modulosmatricula the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Modulosmatricula::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
   
}
