<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MatriculasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="matriculas-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'dni_alumno') ?>

    <?= $form->field($model, 'id_ciclo') ?>

    <?= $form->field($model, 'id_datos_bancarios') ?>

    <?= $form->field($model, 'fecha') ?>

    <?php // echo $form->field($model, 'tipo') ?>

    <?php // echo $form->field($model, 'abona_matricula') ?>

    <?php // echo $form->field($model, 'curso_academico') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
