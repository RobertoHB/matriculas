<?php

namespace app\controllers;

use Yii;
use app\models\Matriculas;
use app\models\MatriculasSearch;
use app\models\Alumnos;
use app\models\Modulos;
use app\models\Modulosmatricula;
use app\models\Datosbancarios;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\helpers\ArrayHelper;
use \Mpdf\Mpdf;
use \yii\helpers\Url;
use \yii\db\Expression;


/**
 * MatriculasController implements the CRUD actions for Matriculas model.
 */
class MatriculasController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Matriculas models.
     * @return mixed
     */
    public function actionIndex()
    {
        
        $searchModel = new MatriculasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        if(isset($_REQUEST['alumno'])){
            
            $alumno = $_REQUEST['alumno'];
            $dataProvider->query->andWhere("dni_alumno = '$alumno'");

            //$modeloAlumno = new Alumnos();


            $query = Alumnos::find()
                    ->select('passnie,dni,nombre,apellidos')
                    ->where(['dni' => $alumno]);

            $datosAlumno =  new ActiveDataProvider([
                'query' => $query,
            ]);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'dniAlumno' =>$alumno,
                'datosAlumno' =>$datosAlumno,

            ]);
        }else{
             return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }
    /**
     * Displays a single Matriculas model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Matriculas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
       //print_r($_REQUEST['alumno']['query']['where']['dni']);
      
        $alumno = $_REQUEST['alumno'];
      
        $model = new Matriculas();
        
        $modeloModulo = new Modulos();
        
        $model_modulos = new Modulosmatricula(); 
        
        $modeloDatosBancarios = new Datosbancarios();
        
        $query = Alumnos::find()
               ->select('passnie,dni,nombre,apellidos')
               ->where(['dni' => $alumno]);
                
        $datosAlumno =  new ActiveDataProvider([
            'query' => $query,
        ]);
        
        $query_modulos = Modulosmatricula::find()
               ->select('id,id_matricula,id_modulo,estado')
         
               ->where(['id_matricula' => $model->id]);
                
        $datosModulos=  new ActiveDataProvider([
            'query' => $query_modulos,
        ]);
        
        
        //controlamos si se estan eviando datos y son de una matricula ya existente. Es decir duplicidad de matricula
        if ($model->load(Yii::$app->request->post()) && $model->getMatriculaduplicada($alumno, $model->id_ciclo, $model->curso_academico)) {
              //controlamos que no se repita la matricula en ciclo y curso academico
             
                    return $this->redirect(['index','alumno'=>$alumno]);
                    exit;
        }
        
        //controlamos si se estan enviando datos y si debe abonar el seguro escolar.Si es mayor de 28 el campo seguro estara a false sino si.
//          if ($model->load(Yii::$app->request->post()) && $model->getSeguro($alumno)){
//            //$_REQUEST['Matriculas']['abona_matricula'] = 1;
//              
//             $model->load(Yii::$app->request->post('Matriculas')['abona_matricula'] = 1);
//              
//          }
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
          
            // guardamos la firma si es el caso
             $model->getGuardarimagen();
             
             //Yii::app()->runController(['matriculas/generarpdf','matricula'=>$model->id,'tipo'=>'F']);
            
            
            //despues de crear la matricula insertamos los modulos que corresponden al ciclo
               //seleccionado en la matricula.
            $modulosCiclo = "SELECT id_modulo FROM modulosciclo WHERE id_ciclo = $model->id_ciclo AND curso = $model->curso";
            
            $dataProvider = new SqlDataProvider([
                    'sql' => $modulosCiclo,        
                ]);

            $resultado = $dataProvider->getModels();
         
            if(count($resultado)>0){

                $data = array();
                foreach($resultado as $value){
                    $modulo = $value['id_modulo'];
                     //Buscamos si existe un modulo anterior matriculado y copiamos el estado en la nueva matricula
                        $ultimoEstado = new Modulosmatricula();
                        $estado = $ultimoEstado->getUltimoestado($model->dni_alumno, $modulo);
                     
                        if($estado == Null){
                            $data[] = [$model->id,$modulo,'NA'];
                        }else{
                             $data[] = [$model->id,$modulo,$estado];
                        }
     
                }
                
               
                Yii::$app->db
                ->createCommand()
                ->batchInsert('modulosmatricula', ['id_matricula','id_modulo','estado'],$data)
                ->execute();
            }   
                return $this->redirect(array_merge(['site/generarpdfs','matricula'=>$model->id,'tipo'=>'F']));
                
                //return $this->redirect(array_merge(['matriculas/generarpdf','matricula'=>$model->id,'tipo'=>'F']));
                 return $this->redirect(['update', 'id' => $model->id,'alumno'=>$alumno]);
                //return $this->redirect(['view', 'id' => $model->id]);
                //return $this->redirect(['index','alumno'=>$alumno]);
                //return $this->redirect(['update', 'id' => $model->id,'alumno'=>$alumno]);
        }

        return $this->render('create', [
            'model' => $model,
            'datosAlumno' => $datosAlumno,
            'datosModulo' => $datosModulos,
            'modeloModulos' => $modeloModulo,
            'modeloDatosBancarios' => $modeloDatosBancarios,
        ]);
    }

    /**
     * Updates an existing Matriculas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id,$alumno)
    {
      
       
//        $model = $this->findModel($id);
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        }
//
//        return $this->render('update', [
//            'model' => $model,
//        ]);
//        $alumno = $_REQUEST[1]['alumno'];
      
        $modeloModulo = new Modulos();
        $model = new Matriculas();
        $model_modulos = new Modulosmatricula(); 
        $modeloDatosBancarios = new Datosbancarios();
        
        $model = $this->findModel($id);
        
        $query = Alumnos::find()
               ->select('passnie,dni,nombre,apellidos')
               ->where(['dni' => $alumno]);
                
        $datosAlumno =  new ActiveDataProvider([
            'query' => $query,
        ]);
        
        $query_modulos = Modulosmatricula::find()
               ->select('id,id_matricula,id_modulo,estado')
               ->where(['id_matricula' => $model->id]);
                
        $datosModulos=  new ActiveDataProvider([
            'query' => $query_modulos,
        ]);
        

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            
            $msg = "Matricula Guardada";
            
            if($model->firma != Null){
                $model->getGuardarimagen();
                
            }
           // Yii::app()->runController(['matriculas/generarpdf','matricula'=>$model->id,'tipo'=>'F']);
//              return $this->redirect(array_merge(['matriculas/generarpdf','matricula'=>$model->id,'tipo'=>'F']));
              
            return $this->redirect(array_merge(['site/generarpdfs','matricula'=>$model->id,'tipo'=>'F']));
             
            return $this->redirect(['update', 'id' => $model->id,'alumno'=>$alumno]);
          
        }

        return $this->render('update', [
            'model' => $model,
            'datosAlumno' => $datosAlumno,
            'datosModulo' => $datosModulos,
            'modeloModulos' => $modeloModulo,
            'modeloDatosBancarios' => $modeloDatosBancarios,
        ]);
    }

   
    
    
    /**
     * Deletes an existing Matriculas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $alumno = $_POST['alumno'];
//        $delete_modulos = Modulosmatricula::find()
//               ->where(['id_matricula' => $id]);
        //eliminamos primero todos los modulos que contenga la matricular a borrar
      Yii::$app->db->createCommand()
        ->delete('modulosmatricula', 'id_matricula ='.$id)
        ->execute();
        //ahora eliminamos la matricula
      
        $this->findModel($id)->delete();

        return $this->redirect(['index','alumno' =>$alumno]);
    }

     public function actionSeguro($alumno)
        {
            $nacimiento = Alumnos::find()
               ->select('f_nac')
               ->where(['dni' => $alumno])
               ->scalar();

            $fecha = time() - strtotime($nacimiento);
            $edad = floor($fecha / 31556926);  
//            return $edad;
            if($edad < 28){
                echo 1;
            }else{
                echo 0;
            }
        }
        public function actionRepiteprimera($alumno,$curso,$ciclo)
       {
            $datos = array();
            $repite = Matriculas::find()
                 ->select('id')
                 ->where(['dni_alumno' => $alumno])
                 ->andWhere(['curso' => $curso])
                 ->andWhere(['id_ciclo' => $ciclo])      
                 ->count();
            $primeraCentro = Matriculas::find()
                           ->select('id')
                           ->where(['dni_alumno' => $alumno])     
                           ->count();
            
           if($primeraCentro > 0){
               $datos['primera'] = 0;
              
           }else{
               $datos['primera'] = 1;
           }
           
           if($repite > 0){
               $datos['repite'] = 1;
           }else{
               $datos['repite'] = 0;
           }
           
           return json_encode($datos);
           
           
       } 
        
        
        
        
//   public function actionGenerarpdf($matricula,$tipo){
//    setlocale(LC_ALL,"es_ES");
//    $dias = array("domingo","lunes","martes","miércoles","jueves","viernes","sábado");
//    $meses = array("","enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre");
//    $fecha = $dias[date("w")].", ".date("d")." de ".$meses[date("n")]." de ".date("Y");
//    $array_cursos = ["1"=>"Primero","2"=>"Segundo"];
//  
//    if(isset($matricula)){
//         $datos = new SqlDataProvider([
//                'sql' => "SELECT a.dni dni,a.nombre nombre,a.apellidos apellidos, a.f_nac fechanaci,a.domicilio domicilio,a.cp cp,
//                            a.localidad localidad, a.tel_fijo fijo, a.movil movil,a.email email,
//                            c.denominacion denominacion,m.curso curso,m.curso_academico cursoacademico 
//                                 FROM matriculas m JOIN alumnos a ON m.dni_alumno = a.dni 
//                                                   JOIN ciclos c ON m.id_ciclo = c.id 
//                                                   WHERE m.id = $matricula",   
//           ]); 
//         
//  
//            $resultado = $datos->getModels();
//            
//            $dni_alumno =  $resultado[0]['dni'];
//            
//            $datos_tutores = new SqlDataProvider([
//                'sql' => " SELECT t.nombre nombreTutor,t.apellidos apeltutor,t.dni dni,t.telefono telef FROM alumnos a JOIN responsables r ON
//                     a.dni = r.dni_alumno JOIN
//                        tutores t ON t.id = r.id_tutor WHERE a.dni = '$dni_alumno'",   
//           ]); 
//            $resultado_tutores = $datos_tutores->getModels();
//            
//            $ruta_firma = url::to('@web/img/alumnos/'.$resultado[0]['dni'].'/'.$matricula.'('.$resultado[0]['cursoacademico'].')/firma.png');
//            $ruta_firma_exi = '../web/img/alumnos/'.$resultado[0]['dni'].'/'.$matricula.'('.$resultado[0]['cursoacademico'].')/firma.png';
//            $ruta_pdf = '../web/img/alumnos/'.$resultado[0]['dni'].'/'.$matricula.'('.$resultado[0]['cursoacademico'].')/';
//            $logo =  url::to('@web/img/logo.png');
//           
////             var_dump($resultado[0]);
////            exit;
//           // $array_datos = json_encode($resultado);
//            if (!file_exists($ruta_firma_exi)){
//                $imagenFirma = "";
//            }else{
//                $imagenFirma = '<img src="'.$ruta_firma.'" width="20%" />';
//            }
//    }
//       
//        
//           $mpdf_doc1 = NEW Mpdf([
//               
//           ]);
////           <!DOCTYPE html>
////                                 <html>
////                                     <head>
////                                         <meta charset="UTF-8">
////                                         <meta name="viewpoort" content="width=device-width,initial-scale=1.0">
////                                     </head>
////                                     <style>
//                $css = '
//                                         div.doc1{
////                                             width:2480px;
////                                             height:3508px;
//                                         }
//                                         div.contenido{
//                                         padding-top:100px;
//                                         }
//                                         p{
//                                             text-align:justify;
//                                         }
//         
//                                         td{
//                                         width:250px;
//                                         }
//                                         .titul_azul{
//                                            color:#8497b0;
//                                         }
//                                         pre{
//                                           color:#8497b0;
//                                         }
//                                         pre span{
//                                         color:black;
//                                         }
//                                          tr.datos td{
//                                         border:1px solid #f7f8fa;
//                                         }
//                                         tr.titulo td{
//                                             color:#8497b0;
//                                         }
//                                         h3{
//                                        
//                                         color:#8497b0;
//                                         }
//                                        h3,h4{
//                                          text-align:center;
//                                         }
//                                       
//                                      
//                                        table.cuerpo2{
//                                           
//                                            border-spacing:3em;
//                                           
//                                          
//                                        }      
//                                      
//                          ';
//                
//                $cabecera1='<img src="'.$logo.'" width="20%"/><br>'
//                                .'<h3 class="titul_azul">DATOS DEL ALUMNO</h3><table class="table">'
//                                .'<tr class="titulo"><td>Nombre</td><td>Apellidos</td><td>Fecha nacimiento</td></tr>'
//                                . '<tr class="datos"><td>'.$resultado[0]['nombre'].'</td><td>'.$resultado[0]['apellidos'].'</td><td>'.$resultado[0]['fechanaci'].'</td></tr>'
//                                . '<tr class="titulo"><td>Domicilio</td><td>Codigo postal</td><td>Localidad</td></tr>'
//                                . '<tr class="datos"><td>'.$resultado[0]['domicilio'].'</td><td>'.$resultado[0]['cp'].'</td><td>'.$resultado[0]['localidad'].'</td></tr>'
//                                . '<tr class="titulo"><td>Telefono domicilio</td><td>Telefono Movil</td><td>Email</td></tr>'
//                                . '<tr class="datos"><td>'.$resultado[0]['fijo'].'</td><td>'.$resultado[0]['movil'].'</td><td>'.$resultado[0]['email'].'</td></tr>'
//                                . '<tr class="titulo"><td>Curso Académico</td><td>Ciclo Formativo</td><td>Curso</td></tr>'
//                                . '<tr class="datos"><td>'.$resultado[0]['cursoacademico'].'</td><td>'.$resultado[0]['denominacion'].'</td><td>'.$array_cursos[$resultado[0]['curso']].'</td></tr>'
//                        . '</table>';
//               
//                $cuerpo = '<div class="doc1"><div class="contenido"><p>El interesado, a través de la suscripción del presente documento presta su consentimiento para que sus datos personales facilitados voluntariamente'
//                     . 'sean tratados, por INSTITUTO DE FORMACION Y CAPACITACION CEINMARK S.L como responsable del tratamiento, con la finalidad de poder utilizar sus datos para '
//                     . 'informarle a usted de aspectos relacionados con la formación que cursa y otras formaciones que pudieran ser de su interés, y conservados durante el periodo'
//                     . 'estipulado por la Consejería de Educación, Formación Profesional y Turismo del Gobierno de Cantabria. Los datos recabados del interesado podrán ser '
//                     . 'comunicados a terceras entidades para el cumplimiento de las obligaciones legales. Del mismo modo declara haber sido informado sobre la posibilidad de'
//                     . 'ejercitar los derechos de acceso, rectificación o supresión de sus datos, dirigiéndose a CALLE VARGAS 65 ENTLO (ACCESO C/PUENTE VIESGO) 39010'
//                     . 'SANTANDER CANTABRIA, asì mismo para obtener información adicional al respecto, podrá consultar la Política de Privacidad en www.ceinmark.net</p><br><br>'
//                     . '<p>Fdo.</p>'.$imagenFirma.'<br>'
//                     . '<p>Nombre y apellidos</p><br>'.$resultado[0]['nombre'].' '.$resultado[0]['apellidos'].'</div></div>';
//                
//                $cuerpo1 = '<h4>Cláusula consentimiento expreso(art.  LOPD)</h4><div class="doc1"><p>De conformidad con lo dispuesto por la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de carácter personal, consiento'
//                        .'que mis datos sean incorporados a n fichero responsabilidad de INSTITUTO DE FORMACION Y CAPACITACION CEINMARK S.L. y que sean tratados con la finalidad de'
//                        .'INDICAR FINALIDAD. Así mismo, declaro haber sido informado sobre la posibilidad de ejercitar los derechos de acceso, rectificación, cancelación y oposición'
//                        .'sobre mis datos, mediante escrito, acompañado de copia del documento oficial que acredite mi identidad, dirigido a INSTITUTO DE FORMACION Y CAPACITACION'
//                        .'CEINMARK S.L. A través de correo electrónico en la dirección ceinmark@ceinmark.net, indicando enla línea de Asunto el derecho que deseo ejercitar, o mediante'
//                        .'correo ordinario remitido a CALLE VARGAS Nº 65 ENTLO(ACCESO C/PUENTE VIESGO), 39010 SANTANDER, CANTABRIA.</p><br><br>'
//                        .'<pre>En Santander, a  '.'<span><b>'.$fecha.'</b></span></pre><br>'
//                        .'<pre>Fdo.</pre>'.$imagenFirma.'<br>'
//                        .'<pre>Fdo. Tutor.-</pre>';
//
//          
//                $cuerpo2 = '<pre><b>               Autorizo            No Autorizo</b></pre>';
//                            $c = 0;
//                            foreach ($resultado_tutores as $valor) {
//                               $c = $c + 1;
//                               $texto_tutor = "A ";
//                               if ($c == 2) $texto_tutor = "Y/o a: ";
//                               $cuerpo2 .=  '<div style="width:2000px"><pre>'.$texto_tutor." ".'<span><b>'.$valor["nombreTutor"]." ".$valor["apeltutor"].'</b></span>           Con D.N.I. nº <b>'.$valor["dni"].'</b></pre></div>
//                                            <div style="width:2000px"><pre>padre/madre/tutor legal, con nº de telefono   <span><b>'.$valor["telef"].'</b></span></pre>';
//                                                             
//                            }
//   
//                            $cuerpo2.= '<br><pre>A representarme ante el citado centro educativo y actuar como interlocutores con el mismo.</pre>
//                                        <br><pre>En Santander, a  <span><b>'.$fecha.'</b></span></pre>
//                                        <br><pre>Fdo.</pre>
//                                        <pre>'.$imagenFirma.'</pre>
//                                        <pre>Fdo.-                               Fdo.-</pre>';
////                            
////                 $cuerpo2 = '<div class="container"><div class="row">'
////                            . '<div class="col-md-6">Autorizo</div><div class="col-md-6">No Autorizo</div></div>'
////                            . '<div class="row">'
////                            . '<div class="col-md-2">A </div><div class="col-md-4">Sara Blanco Pérez</div><div class="col-md-3">Con D.N.I. nº</div><div class="col-md-3">13168589C</div></div></div></html>';
//////                            . '<tr><td>padre/madre/tutor legal, con nº de telefono</td><td>942371228</td></tr>'
//////                            . '<tr><td>Y/o a: </td><td>Fulanito de tal </td><td>con D.N.I. nº</d><td> 25125478A</td></tr>'
//////                            . '<tr><td>padre/madre/tutor legal, con nº de telefono</td><td>689587458</td></tr>'
//////                            . '<tr><td>A representarme ante el citado centro educativo y actuar como interlocutores con el mismo.</td></tr>'
//////                            . '<tr><td>En Santander, a</td><td>martes, 25 de febrero de 2020</td></tr>'
//////                            .'<tr><td>Fdo.</td></tr>'
//////                            .'<tr><td>Fdo.-</td><td>Fdo.-</td></tr></table></html>';
//                
//                $contenido = $cuerpo;
//                $contenido1 = $cabecera1.$cuerpo1;
//                $contenido2 = $cabecera1.$cuerpo2;
//                
//            $mpdf_doc1->keep_table_proportions = true;
//            $mpdf_doc1->shrink_tables_to_fit = 1;
//            $mpdf_doc1->use_kwt = true; 
//            $mpdf_doc1->WriteHTML($css,\Mpdf\HTMLParserMode::HEADER_CSS);
//            $mpdf_doc1->WriteHTML($contenido,\Mpdf\HTMLParserMode::HTML_BODY); 
//            $mpdf_doc1->AddPage();
//            $mpdf_doc1->WriteHTML($contenido1,\Mpdf\HTMLParserMode::HTML_BODY);   
//            $mpdf_doc1->AddPage();
//            $mpdf_doc1->WriteHTML($contenido2,\Mpdf\HTMLParserMode::HTML_BODY);
//            if ($tipo == 'V'){
//                $mpdf_doc1->Output(); 
//            }else{
//                 if(file_exists($ruta_pdf.'firma.pdf')){
//                    $nombre_fichero = $ruta_pdf.'firma.pdf';
//                    unlink($nombre_fichero);
//                }    
//                $mpdf_doc1->Output($ruta_pdf.'firma.pdf',\Mpdf\Output\Destination::FILE);
//                 return $this->redirect(['update', 'id' => $matricula,'alumno'=>$dni_alumno]);
//               
//            }
//            return $this->redirect(Yii::$app->request->referrer);
//
//
//   } 

    /**
     * Finds the Matriculas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Matriculas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Matriculas::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
